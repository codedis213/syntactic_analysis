
# coding: utf-8

# In[2]:


from nltk import word_tokenize, pos_tag


# In[61]:


class DoSyntacticAnalysis(object):
    
    def __init__(self):
        self.sentence = None
        self.negative_word = ["not", "never", "n't"]
        self.verb_tags_list = ["VB", "VBD", "VBG", "VBN", "VBP", "VBZ", "MD"]
        self.tokens = None 
        self.token_tag_list = None 
        self.first_found_verb = None 
        self.first_found_token = None
        self.first_verb_index = None
        
        self.first_found_noun = None 
        self.first_found_noun_token = None
        self.first_found_noun_index = None 
        
        self.second_found_verb = None 
        self.second_found_token = None
        self.second_verb_index = None
        
        self.final_sentence = None
        self.is_introgative = False 
        
        self.noun_pronoun_list = ["NN", "NNS", "NNP", "NNPS", "PRP", "PRP$", "WP", "WP$"]
        
        self.auxiliary_verb = ['do','does','did','has','have','had','is','am','are','was',
                                 'were','be','being','been','may','must','might','should','could',
                                 'would','shall','will','can', 'need', 'dare', "ca", "sha", "dare", "ought", "wo",
                              "ai"]
        self.is_auxiliary_verb = False 
        
        self.negate_aux = {'ai': "ain't", 'am': 'am not', 'are': 'are not', 'be': 'not be','been': 'not been',
                           'being': 'not being', 'ca': "can't", 'can': 'cannot', 'could': 'could not',
                           'dare': 'not dare', 'did': 'did not', 'do': 'do not', 'does': 'does not', 
                           'had': 'had not', 'has': 'has not', 'have': 'have not', 'is': 'is not', 
                           'may': 'may not', 'might': 'might not', 'must': 'must not', 'need': 'need not',
                           'ought': 'ought not', 'sha': "shan't", 'shall': 'shall not', 'should': 'should not',
                           'was': 'was not', 'were': 'were not', 'will': 'will not', 'wo': "won't",
                           'would': 'would not'}
        
    def reinitialize(self):
        self.tokens = None 
        self.token_tag_list = None 
        self.first_found_verb = None 
        self.first_found_token = None
        self.first_verb_index = None
        self.is_auxiliary_verb = False 
        self.final_sentence = None
        self.is_introgative = False
        self.first_found_noun = None 
        self.first_found_noun_token = None
        self.first_found_noun_index = None 
        self.second_found_verb = None 
        self.second_found_token = None
        self.second_verb_index = None
        
        
    def rotate_on_token_tag(self):
        index = 0 
        
        for token, tag in self.token_tag_list:
            if tag in self.verb_tags_list:
                self.first_found_token =  token
                self.first_found_verb = tag 
                self.first_verb_index = index
                if token in self.auxiliary_verb:
                    self.is_auxiliary_verb = True
                return 
            index += 1
            
    def search_first_pronoun(self, token_tag_list):
    
        first_token, first_tag = token_tag_list[0]
        second_token, second_tag = token_tag_list[1]
        last_token, last_tag = token_tag_list[-1]
        
        print([first_token, first_tag,second_token, second_tag, last_token, last_tag ])
        
        self.second_found_verb = None 
        self.second_found_token = None
        self.second_verb_index = None 
        
        index = 0 
        loop = True
        length = token_tag_list.__len__()
        
        while loop and index <= length-1:
            token, tag = token_tag_list[index]
    
            if tag in self.noun_pronoun_list:
                self.first_found_noun =  True
                self.first_found_noun_token = token
                self.first_found_noun_index = index
                loop = False 
                
            index += 1
            
        loop = True
        
        while loop and index <= length-1:
            token, tag = token_tag_list[index]
    
            if tag in self.verb_tags_list:
                self.second_found_verb =  True
                self.second_found_token = token
                self.second_verb_index = index
                loop = False 
                
            index += 1
            
        if first_token in self.auxiliary_verb and second_token in self.negative_word:
            pass
        
        elif self.first_found_noun and self.second_found_verb:
            if self.tokens[self.second_verb_index -1] in self.negative_word:
                pass 
            else:
                self.tokens[self.second_verb_index] = "not %s" % self.second_found_token
                
        elif first_token in self.auxiliary_verb and second_token  not in self.negative_word:
            print(second_token)
            self.tokens[0] = "%sn't" % first_token
            
        self.final_sentence = " ".join(self.tokens)
        self.final_sentence = self.final_sentence.replace(" n't", "n't")
        print(self.final_sentence)
            
            
    
    def negate_introgative(self, token_tag_list):
        print(token_tag_list)
        if token_tag_list:
            first_token, first_tag = token_tag_list[0]
            second_token, second_tag = token_tag_list[1]
            last_token, last_tag = token_tag_list[-1]

            if last_token == "?":
                self.is_introgative = True
                self.search_first_pronoun(token_tag_list)

            else:
                self.is_introgative = False

    def doNegative(self):
        if  not self.is_introgative:
            self.rotate_on_token_tag()
            if not self.first_found_verb:
                print(self.sentence)
                print("not found verbs")
            self.checkNegative()
        
            
    def usePosTag(self, sentence):
        self.sentence = sentence.lower()
        self.tokens = tokens = word_tokenize(self.sentence)
        self.token_tag_list = pos_tag(tokens)
        self.negate_introgative(self.token_tag_list)
           
            
    def imperative_pos_tag(self, sent):
        return [(word, tag[:2]) if tag.startswith('VB') else (word,tag) for word, tag in pos_tag(['He']+sent)[1:]]
    
            
    def checkNegative(self):
        if self.first_found_verb:
            if self.is_auxiliary_verb and self.first_found_verb:
                token_tag = self.token_tag_list[self.first_verb_index+1]
            elif self.first_found_verb:
                token_tag = self.token_tag_list[self.first_verb_index-1]

            token, tag = token_tag

            result = False

            if token in self.negative_word:
                result =  True

            elif self.is_auxiliary_verb:
                self.tokens[self.first_verb_index] =  self.negate_aux[self.first_found_token]

            elif self.first_found_verb in ["VB", "VBP"]:
                self.tokens[self.first_verb_index] = "do not %s" % self.first_found_token

            elif self.first_found_verb in ["VBD"]:
                self.first_found_token = self.first_found_token.replace("ed", "")
                self.tokens[self.first_verb_index] = "did not %s" % self.first_found_token

            elif self.first_found_verb in ["VBZ"]:
                if self.first_found_token.endswith("es"):
                    self.first_found_token = self.first_found_token[:-2]
                elif self.first_found_token.endswith("s"):
                    self.first_found_token = self.first_found_token[:-1]
                self.tokens[self.first_verb_index] = "does not %s" % self.first_found_token

            self.final_sentence = " ".join(self.tokens)
            self.final_sentence = self.final_sentence.replace(" n't", "n't")
            print(self.final_sentence)
            return result   


# In[70]:


import re 

obj = DoSyntacticAnalysis()
sentences =  """All the cars stop at this crossing.That notice reads, “No parking”.The teacher said if she works hard she will pass.There goes the taxi.Does he write novels?Do you play tennis?He does not go to the temple.Bill jogs every day.Bill never jogs.Does Bill jog on Sundays?Jane works hard.Jane doesn't work hard at all!Jim builds houses for a living.What does Jim do for a living?They play basketball every Sunday.At what time do you usually eat dinner?London is in England.London is not in France.You can't live without water.Can you live without water?Sarah is a good girl.When does it start snowing?I speak Japanese.I don't speak French.I agree.

Tom thinks it's a good idea.They have a lot of money.They don't have any money.Do they have some money?I feel so tired.You are brilliant!They don't need his help.The action is not only occurring now; it repeats after regular intervals of time. my name is jai. When do you want to go to bed? Is your  name jai ? are you playing? I am not a good boy. I am not playing.I amn't playing."""
sentence_list = re.split("([.!?]+)", sentences)


start = 0
for index in range(0, sentence_list.__len__(), 2):
    sentence = "".join(sentence_list[start:index])
    print(sentence)
    start = index
    obj.reinitialize()
    obj.usePosTag(sentence)
    obj.doNegative()
    print("*"*30)


# In[ ]:



             
    

